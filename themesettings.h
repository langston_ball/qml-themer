#ifndef THEMESETTINGS_H
#define THEMESETTINGS_H

#include <QObject>
#include "qsettings.h"

class ThemeSettings : public QObject
{
    Q_OBJECT
public:
    explicit ThemeSettings(QObject *parent = 0);

signals:

public slots:
    QString reset();
    QString getSkin();
    QString getTheme();
    QVariant getSkinProperty(QString key);

    void setSkin(QString skin);
    void setTheme(QString theme);

private:
    QSettings *m_settings;

private slots:
    void clearThemeSettings();
    void setupThemeSettings();

};

#endif // THEMESETTINGS_H
