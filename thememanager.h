#ifndef THEMEMANAGER_H
#define THEMEMANAGER_H

#include <QObject>
#include "screeninfo.h"
#include "themesettings.h"
#include "qqmlapplicationengine.h"
#include "qfileselector.h"
#include "qquickwindow.h"

class ThemeManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ready READ ready WRITE setReady NOTIFY readyChanged)
    /*! Theme relates to a sub setting of a skin */
    Q_PROPERTY(QString theme READ theme WRITE setTheme NOTIFY themeChanged)
    /*! Skin relates to the overall ui setup for the app */
    Q_PROPERTY(QString skin READ skin WRITE setSkin NOTIFY skinChanged)
    /*! The Current language Setting */
    Q_PROPERTY(QString language READ language WRITE setLanguage NOTIFY languageChanged)

    Q_PROPERTY(bool reloading READ reloading WRITE setReloading NOTIFY reloadingChanged)
    Q_PROPERTY(int windowHeight READ windowHeight WRITE setWindowHeight NOTIFY windowHeightChanged)
    Q_PROPERTY(int windowWidth READ windowWidth WRITE setWindowWidth NOTIFY windowWidthChanged)
    Q_PROPERTY(ScreenInfo* screenInfo READ screenInfo NOTIFY screenInfoChanged)
public:
    explicit ThemeManager(QQmlApplicationEngine* appEngine, QObject *parent = 0);

    bool ready() const { return m_ready; }
    QString theme() const { return m_theme; }

    QString fontsPath() const;
    void setFontsPath(const QString &fontsPath);

    QString themePath() const;
    void setThemePath(const QString &themePath);


    QString language() const;
    QString skin() const;

    Q_INVOKABLE void setupTheme();
    bool reloading() const;

    int windowHeight() const
    {
        return m_windowHeight;
    }

    int windowWidth() const
    {
        return m_windowWidth;
    }

    ScreenInfo* screenInfo() const
    {
        return m_screenInfo;
    }

signals:

    void readyChanged(bool ready);
    void themeChanged(QString theme);    
    void languageChanged(QString language);
    void skinChanged(QString skin);

    void reloadingChanged(bool reloading);

    void windowHeightChanged(int windowHeight);

    void windowWidthChanged(int windowWidth);

    void screenInfoChanged(ScreenInfo* screenInfo);

public slots:
    void reloadQml();
    void handleScreenInfo();
    void setReady(bool ready);
    void loadMainQmlFile();

    void setTheme(QString theme);    
    void setLanguage(QString language);
    void setSkin(QString skin);

    Q_INVOKABLE QString selectImage(QString path);

    void handleObjectsCreated(QObject *object, const QUrl &url);

    void setReloading(bool reloading);

    void setWindowHeight(int windowHeight)
    {
        if (m_windowHeight == windowHeight)
            return;

        m_windowHeight = windowHeight;
        emit windowHeightChanged(windowHeight);
    }

    void setWindowWidth(int windowWidth)
    {
        if (m_windowWidth == windowWidth)
            return;

        m_windowWidth = windowWidth;
        emit windowWidthChanged(windowWidth);
    }

private slots:
    void delayedReloadQml();


private:
    QQmlApplicationEngine *m_appEngine;
    ScreenInfo *m_screenInfo;

    bool m_simulated;
    bool m_ready;
    QString m_theme;
    QString m_fontsPath;
    QString m_themePath;
    QObject *m_themeStyle;
    QObject *m_fonts;
    QString m_language;
    QString m_skin;
    ThemeSettings *m_settings;
    QFileSelector *m_selector;
    QVariantList m_themeList;
    QQuickWindow *m_window;
    bool m_reloading;
    int m_windowHeight;
    int m_windowWidth;
};

#endif // THEMEMANAGER_H
