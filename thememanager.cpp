#include "thememanager.h"
#include "qqmlcontext.h"
#include "qqmlfileselector.h"
#include "qdir.h"
#include "qdebug.h"
#include "qqmlcomponent.h"
#include "qjsondocument.h"
#include "qjsonarray.h"
#include "qjsonobject.h"
#include "qtimer.h"

ThemeManager::ThemeManager(QQmlApplicationEngine *appEngine, QObject *parent) : QObject(parent)
  , m_appEngine(appEngine)
  , m_screenInfo(new ScreenInfo())
  , m_themeStyle(0)
  , m_settings(new ThemeSettings())
  , m_window(0)
{
#ifdef SIMULATED
    m_simulated = true;
#else
    m_simulated = false;

#ifdef NO_ANDROID
    m_windowHeight =480;
    m_windowWidth = 640;

#else
    m_windowHeight = screenInfo()->primaryScreen()->height();
    m_windowWidth= screenInfo()->primaryScreen()->width();
#endif

#endif
    m_reloading = false;
    m_selector = new QFileSelector(m_appEngine->rootContext()->engine());
    m_skin = m_settings->getSkin();
    m_theme= m_settings->getTheme();
    m_themePath = QString("qrc:/qml/%1Theme.qml").arg(m_skin);
    m_fontsPath = QString("qrc:/qml/%1Fonts.qml").arg(m_skin);

    connect(m_screenInfo, SIGNAL(screenSizeChanged()), this, SLOT(handleScreenInfo()));
    connect(m_appEngine, SIGNAL(objectCreated(QObject*,QUrl)), this, SLOT(handleObjectsCreated(QObject*,QUrl)));

    m_appEngine->rootContext()->setContextProperty("themeManager", this);
    m_appEngine->rootContext()->setContextProperty("screenInfo", m_screenInfo);


    QFile themeListFile(QString(":/qml/%1Themes.json").arg(m_skin));
    if(themeListFile.exists()){
        themeListFile.open(QFile::ReadOnly);
        //  QJsonDocument obj = QJsonDocument::fromBinaryData(themeListFile.readAll());

        QJsonObject themes = QJsonDocument::fromJson(themeListFile.readAll()).object();
        QJsonArray themeArr = themes.value("themes").toArray();
        qDebug() << themeArr.count() << " themes found";
        m_themeList = themeArr.toVariantList();
        if(themeArr.count()==0){
            qDebug() <<  "From " << themeListFile.fileName() << " \n" <<   themeListFile.readAll();
        } else {
            if(m_themeList.contains(m_theme)){
                setTheme(m_theme);

            } else {
                setTheme(m_themeList.at(0).toString());
            }

            setupTheme();
        }

    } else {
        qDebug() << " Could not find themes in " << themeListFile.fileName();
    }
    m_appEngine->rootContext()->setContextProperty("themeList", QVariant::fromValue(m_themeList));

}

void ThemeManager::reloadQml()
{
    QMetaObject::invokeMethod(this, "delayedReloadQml", Qt::QueuedConnection);
}

void ThemeManager::handleScreenInfo()
{
    QStringList selectorParams;
    selectorParams <<m_theme << m_screenInfo->primaryScreen()->deviceSizeString() <<m_screenInfo->primaryScreen()->pixelDensityString();
    m_selector->setExtraSelectors(selectorParams);
    delayedReloadQml();
}

void ThemeManager::setReady(bool ready)
{
    if (m_ready == ready)
        return;

    m_ready = ready;
    emit readyChanged(ready);
}

void ThemeManager::delayedReloadQml()
{

    if(m_window !=0){
        qDebug() << "reloading";
        m_appEngine->load(QString(":/qml/%1QmlReload.qml").arg(m_skin));
        m_appEngine->clearComponentCache();
        QTimer::singleShot(1000, Qt::CoarseTimer, this, SLOT(setupTheme()) );
    } else {
        setupTheme();
    }

}

QString ThemeManager::themePath() const
{
    return m_themePath;
}

void ThemeManager::setThemePath(const QString &themePath)
{
    m_themePath = themePath;
}

void ThemeManager::setupTheme()
{
    QStringList selectorParams;
    if(!m_theme.isEmpty())
        selectorParams << m_theme;

    selectorParams <<  m_screenInfo->primaryScreen()->pixelDensityString() << m_screenInfo->primaryScreen()->deviceSizeString() ;
    m_selector->setExtraSelectors(selectorParams);
    QQmlFileSelector * qmlSelector = QQmlFileSelector::get(m_appEngine->rootContext()->engine());
    qmlSelector->setSelector(m_selector);
    qDebug() << m_selector->allSelectors();

    QQmlComponent *fontComponent = new QQmlComponent(m_appEngine->rootContext()->engine(), QUrl(m_selector->select(m_fontsPath)), QQmlComponent::PreferSynchronous);
    m_fonts = fontComponent->create();

    if(fontComponent)
        m_appEngine->rootContext()->setContextProperty("Fonts", m_fonts);
    else
        qDebug() <<"Font Component Error" << fontComponent->errorString();

    QQmlComponent *themeComponent = new QQmlComponent(m_appEngine->rootContext()->engine(), QUrl(m_selector->select(m_themePath)), QQmlComponent::PreferSynchronous);
    m_themeStyle = themeComponent->create();
    qDebug() << themeComponent->url();

    if(themeComponent)
        m_appEngine->rootContext()->setContextProperty("Theme", m_themeStyle);
    else
        qDebug()<< "Theme Component Error" << themeComponent->errorString();


    loadMainQmlFile();

}

bool ThemeManager::reloading() const
{
    return m_reloading;
}

QString ThemeManager::language() const
{
    return m_language;
}

QString ThemeManager::skin() const
{
    return m_skin;
}

QString ThemeManager::fontsPath() const
{
    return m_fontsPath;
}

void ThemeManager::setFontsPath(const QString &fontsPath)
{
    m_fontsPath = fontsPath;
}

void ThemeManager::loadMainQmlFile()
{

    if(m_window){
        setReady(true);
    } else {
        QString entryFile = QString("qrc:/qml/%1main.qml").arg(m_skin);
        m_appEngine->setBaseUrl(QUrl(QString("qrc:/qml/%1").arg(m_skin)));

        qDebug() << "Base url for engine " << m_appEngine->baseUrl().toString();
        m_appEngine->load( QUrl((entryFile)));
    }
}

void ThemeManager::setTheme(QString theme)
{
    if (m_theme == theme)
        return;
    setReady(false);
    m_theme = theme;
    emit themeChanged(theme);
    delayedReloadQml();
}

void ThemeManager::setLanguage(QString language)
{
    if (m_language == language)
        return;

    m_language = language;
    emit languageChanged(language);
}

void ThemeManager::setSkin(QString skin)
{
    if (m_skin == skin)
        return;

    m_skin = skin;
    emit skinChanged(skin);
}

QString ThemeManager::selectImage(QString path)
{

    QString fullpath = QString("qrc:/qml/%1%2").arg(m_skin).arg(path);

    QString res  = m_selector->select(fullpath);
    if(res==fullpath){
        qDebug() << "override not found ";
        qDebug() << m_selector->extraSelectors();
    }
    qDebug() << QString("Incoming path %1 :: Outgoing path %2").arg(fullpath).arg(res);
    return res;
}

void ThemeManager::handleObjectsCreated(QObject *object, const QUrl &url)
{
    if(url.toString()== QString("qrc:/qml/%1main.qml").arg(m_skin)){
        m_window = qobject_cast<QQuickWindow *>(object);

        if(m_window==NULL)
            return;

        qDebug() << "captured main window";
        connect(m_window, SIGNAL(heightChanged(int)), this, SLOT(setWindowHeight(int)));
        connect(m_window, SIGNAL(widthChanged(int)), this, SLOT(setWindowWidth(int)));
        setWindowHeight(m_window->height());
        setWindowWidth(m_window->width());
    }
}

void ThemeManager::setReloading(bool reloading)
{
    if (m_reloading == reloading)
        return;

    m_reloading = reloading;
    emit reloadingChanged(reloading);
}
