 


HEADERS += \
    $$PWD/thememanager.h \
    $$PWD/screeninfo.h \
    $$PWD/themesettings.h

SOURCES += \
    $$PWD/thememanager.cpp \
    $$PWD/screeninfo.cpp \
    $$PWD/themesettings.cpp

RESOURCES += \
    $$PWD/themeresources.qrc

