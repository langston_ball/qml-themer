#include "themesettings.h"

ThemeSettings::ThemeSettings(QObject *parent) : QObject(parent),
    m_settings(new QSettings())
{

}

QString ThemeSettings::reset()
{
    clearThemeSettings();
}

QString ThemeSettings::getSkin()
{
    m_settings->beginGroup("themesettings");
    if(m_settings->contains("skin"))
        return m_settings->value("skin").toString().append("/");
    else
        return "";
}

QString ThemeSettings::getTheme()
{
    m_settings->beginGroup("themesettings");
    if(m_settings->contains("theme"))
        return m_settings->value("theme").toString();
    else
        return "";
}

QVariant ThemeSettings::getSkinProperty(QString key)
{
    m_settings->beginGroup("themesettings");
    if(m_settings->contains(key))
        return m_settings->value(key).toString();
    else
        return "";
}

void ThemeSettings::setSkin(QString skin)
{
    m_settings->beginGroup("themesettings");
    m_settings->setValue("skin", skin);
    m_settings->endGroup();
}

void ThemeSettings::setTheme(QString theme)
{
    m_settings->beginGroup("themesettings");
    m_settings->setValue("theme", theme);
    m_settings->endGroup();
}

void ThemeSettings::clearThemeSettings()
{
    m_settings->remove("themesettings");
}

void ThemeSettings::setupThemeSettings()
{
    if(m_settings){
        if(!m_settings->childGroups().contains("themesettings")){
            m_settings->beginGroup("themesettings");
            m_settings->setValue("skin", "default");
            m_settings->setValue("theme", "");
        }
    }
}
